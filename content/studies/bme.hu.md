---
title: "Budapesti Műszaki és Gazdaságtudományi Egyetem Villamosmérnöki Kar"
dateRange: "2013 - 2017"
draft: false
---

Villamosmérnöki szakon kezdtem BSC tanulmányaimat. 
Programozói tudásom és látásmódom alapjait mikrokontrollerek és ehhez kapcsolódó vastagkliensek fejesztésével szereztem.