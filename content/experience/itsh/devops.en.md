---
title: "DevOps tooling operator and process developer"
weight: 3
draft: false
---

Installation and maintenance of Continuous Integration stack.
Migration of development processes and knowledge sharing.

    * Gitlab
    * Jenkins
    * SonarQube
    * Docker & Kubernetes
    * Nexus Repository Manager
    * Sentry
