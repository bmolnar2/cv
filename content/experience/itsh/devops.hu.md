---
title: "DevOps tooling üzemeltető és folyamat fejlesztő"
weight: 3
draft: false
---

Continuous Integration stack üzembehelyezése és karbantartása.
Fejlesztési folyamatok átszervezése és tudásmegosztás az eszközökről.

    * Gitlab
    * Jenkins
    * SonarQube
    * Docker & Kubernetes
    * Nexus Repository Manager
    * Sentry
