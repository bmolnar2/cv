---
title: "Operations and Linux administrative skills"
weight: 4
draft: false
---

Java application servers (Jboss, Tomcat),
reverse proxy (Apache, Nginx),
bare metal Kubernetes
